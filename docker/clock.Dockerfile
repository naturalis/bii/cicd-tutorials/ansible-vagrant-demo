FROM ubuntu:noble

RUN mkdir -p "/opt/apps"

COPY clock.sh /opt/apps

RUN chmod +x /opt/apps/clock.sh

ENTRYPOINT [ "/opt/apps/clock.sh" ]