FROM ubuntu:noble

RUN mkdir -p "/opt/apps"

COPY secret.sh /opt/apps

RUN chmod +x /opt/apps/secret.sh

ENTRYPOINT [ "/opt/apps/secret.sh" ]