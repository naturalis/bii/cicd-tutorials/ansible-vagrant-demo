#!/bin/bash

set -e

# Are we running inside a Gitlab pipeline?
GITLAB_PIPELINE=true
[[ -z ${CI_JOB_ID+x} ]] && GITLAB_PIPELINE=false

if [[ ${GITLAB_PIPELINE} == true ]]; then
  BASE_DIR="${CI_PROJECT_DIR}"
else
  BASE_DIR=$(dirname "${0}")/../
  BASE_DIR=$(realpath "${BASE_DIR}")
fi

echo "Tagging Docker image created for Git commit \"${CI_COMMIT_SHORT_SHA}\""

image="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}"

docker pull "${image}"

echo '======> Authenticating GitLab container registry'
echo -n ${CI_REGISTRY_PASSWORD} | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}

if [[ -n ${CI_COMMIT_TAG+x} ]]; then
  echo "======> Adding tag \"${CI_COMMIT_TAG}\""
  docker tag "${image}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
  docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
  exit 0
fi

echo "======> Adding tag \"${CI_COMMIT_REF_SLUG}\""
docker tag "${image}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"

tag=$(date '+%Y_%m_%d')
echo "======> Adding tag \"${tag}\""
docker tag "${image}" "${CI_REGISTRY_IMAGE}:${tag}"
docker push "${CI_REGISTRY_IMAGE}:${tag}" &> /dev/null

if [[ ! -z ${IMAGE_TAG+x} && ! -z ${IMAGE_TAG} ]]; then
  tag=${IMAGE_TAG}
  echo "======> Adding tag \"${tag}\""
  docker tag "${image}" "${CI_REGISTRY_IMAGE}:${tag}"
  docker push "${CI_REGISTRY_IMAGE}:${tag}"
fi

if [[ "${CI_COMMIT_REF_SLUG}" = "main" ]]; then
  echo "======> Adding tag \"latest\""
  docker tag "${image}" "${CI_REGISTRY_IMAGE}:latest"
  docker push "${CI_REGISTRY_IMAGE}:latest"
fi
