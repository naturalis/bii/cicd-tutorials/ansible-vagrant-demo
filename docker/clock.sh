#!/bin/bash

# Print the time every 3 seconds
while [[ true ]]; do
  date +%T
  sleep 3
done