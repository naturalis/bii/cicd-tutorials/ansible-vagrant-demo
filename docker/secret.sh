#!/bin/bash

# Print the GREETING secret every 3 seconds (don't do this at home)
while [[ true ]]; do
  echo $GREETING
  sleep 3
done