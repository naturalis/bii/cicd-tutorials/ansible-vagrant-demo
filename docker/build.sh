#!/bin/bash

# Executes a local build. This script is not part of the GitLab
# pipeline, but rather mimics it.
#
# Afterwards you can manually "docker compose pull" the image on
# a server of your choosing (see also ansible/deploy.sh).
#
# This script is set up as though it _might_ itself be run in a
# GitLab pipeline. In practice, though, the more granular scripts
# invoked by build.sh (create-image.sh, tag-image.sh) are kicked 
# off separately in the GitLab pipeline.


# Are we running inside a GitLab pipeline?
GITLAB_PIPELINE=true
[[ -z ${CI_JOB_ID+x} ]] && GITLAB_PIPELINE=false

if [[ ${GITLAB_PIPELINE} == true ]]; then
  BASE_DIR="${CI_PROJECT_DIR}"
else
  BASE_DIR=$(dirname "${0}")/../
  BASE_DIR=$(realpath "${BASE_DIR}")
fi

if [[ ${GITLAB_PIPELINE} == false ]]; then
  f="${BASE_DIR}/docker/build.env"
  if [[ ! -f "${f}" ]]; then
    echo 'For locally executed builds, the following file is required:'
    echo 'docker/build.env.'
    exit 1
  fi
  echo 'Setting up environment for local build'
  source "${f}"
fi

if [[ ${GITLAB_PIPELINE} == false ]]; then
  echo 'Validating local environment'
  if [[ -z ${CI_REGISTRY+x} ]]; then
    echo 'Missing environment variable: CI_REGISTRY. Check docker/build.env' && exit 1
  fi
  if [[ -z ${CI_REGISTRY_IMAGE+x} ]]; then
    echo 'Missing environment variable: CI_REGISTRY_IMAGE. Check docker/build.env' && exit 1
  fi
  if [[ -z ${CI_REGISTRY_USER+x} ]]; then
    echo 'Missing environment variable: CI_REGISTRY_USER. Check docker/build.env' && exit 1
  fi
  if [[ -z ${CI_REGISTRY_PASSWORD+x} ]]; then
    echo 'Missing environment variable: CI_REGISTRY_PASSWORD. Check docker/build.env' && exit 1
  fi
  echo 'Auto-computing other GitLab CI variables'
  CI_COMMIT_REF_SLUG="$(git rev-parse --abbrev-ref HEAD)"
  CI_COMMIT_SHA="$(git log -n 1 --pretty=format:'%H')"
  CI_COMMIT_SHORT_SHA="$(git log -n 1 --pretty=format:'%h')"
  echo "======> CI_COMMIT_REF_SLUG ....: ${CI_COMMIT_REF_SLUG}"
  echo "======> CI_COMMIT_SHA .........: ${CI_COMMIT_SHA}"
  echo "======> CI_COMMIT_SHORT_SHA ...: ${CI_COMMIT_SHORT_SHA}"
fi

source "${BASE_DIR}/docker/create-image.sh"
source "${BASE_DIR}/docker/tag-image.sh"

echo "Build successful"
